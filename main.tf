provider "aws" {
  region = var.aws_region
}

#Deploy Keys For Resources
module "keypair" {
  source     = "./modules/keypair"
  key_name   = var.key_name
  public_key = var.public_key

}
#Deploy Security Group For Resources
module "securitygroups" {
  source              = "./modules/securitygroups"
  vpc_cidr            = var.vpc_cidr
  vpc_id              = module.networking.vpc_id
  ingress_cidr_blocks = var.ingress_cidr_blocks
}

# Deploy Networking Resources
module "networking" {
  source          = "./modules/networking"
  vpc_cidr        = var.vpc_cidr
  subnet1_az      = var.subnet1_az
  subnet1_cidr    = var.subnet1_cidr
  subnet2_az      = var.subnet2_az
  subnet2_cidr    = var.subnet2_cidr
  subnet3_az      = var.subnet3_az
  subnet3_cidr    = var.subnet3_cidr
  subnet4_az      = var.subnet4_az
  subnet4_cidr    = var.subnet4_cidr
  subnet5_az      = var.subnet5_az
  subnet5_cidr    = var.subnet5_cidr
  subnet6_az      = var.subnet6_az
  subnet6_cidr    = var.subnet6_cidr
  nat_instance_id = module.compute.nat_instance_id
}


#Deploy Database Resources
module "database" {
  source                  = "./modules/database"
  database_engine_version = var.database_engine_version
  db_identifier           = var.db_identifier
  allocated_storage       = var.allocated_storage
  storage_type            = var.storage_type
  database_engine         = var.database_engine
  instance_class          = var.instance_class
  db_name                 = var.db_name
  db_username             = var.db_username
  db_password             = var.db_password
  db_security_group       = module.securitygroups.aws_db_security_group_id
  deletion_window_in_days = var.deletion_window_in_days
  license_model           = var.license_model
  maintenance_window      = var.maintenance_window
  monitoring_interval     = var.monitoring_interval
  backup_window           = var.backup_window
  backup_retention_period = var.backup_retention_period
  subnet_ids              = [module.networking.aws_subnet6_id, module.networking.aws_subnet5_id]
}

# Deploy Compute Resources
module "compute" {
  source                     = "./modules/compute"
  key_name                   = module.keypair.key_pair_name
  ec2_instance_type          = var.ec2_instance_type
  ec2_ami                    = var.ec2_ami
  vpc_security_group_ids     = [module.networking.default_security_group_id]
  subnet_id1                 = module.networking.aws_subnet3_id
  subnet_id2                 = module.networking.aws_subnet4_id
  ec2_volume_size            = var.ec2_volume_size
  ec2_volume_type            = var.ec2_volume_type
  availability_zone1         = var.availability_zone1
  availability_zone2         = var.availability_zone2
  root_volume_size           = var.root_volume_size
  root_volume_type           = var.root_volume_type
  role                       = module.roles.webserver_role_name
  ebs_block_name             = var.ebs_block_name
  ebs_optimised              = var.ebs_optimised
  nat_ami                    = var.nat_ami
  nat_instance_type          = var.nat_instance_type
  nat_volume_size            = var.nat_volume_size
  nat_volume_type            = var.nat_volume_type
  nat_vpc_security_group_ids = [module.securitygroups.nat_id]
  nat_subnet_id              = module.networking.aws_subnet1_id
}

#IAM Role And Policies
module "roles" {
  source = "./modules/roles"
}
module "policies" {
  source          = "./modules/policies"
  webservers_role = module.roles.webservers_role_id
}


#Deploy Cache Resources
module "cache" {
  source                     = "./modules/cache"
  engine                     = var.engine
  cluster_id                 = var.cluster_id
  node_type                  = var.node_type
  engine_version             = var.engine_version
  cluster_maintenance_window = var.cluster_maintenance_window
  cluster_availability_zone  = var.cluster_availability_zone
  num_cache_nodes            = var.num_cache_nodes
  parameter_group_name       = var.parameter_group_name
  subnet_ids1                = [module.networking.aws_subnet3_id, module.networking.aws_subnet4_id]
}

#Deploy LoadBalancer Resources
module "loadbalancer" {
  source             = "./modules/loadbalancer"
  lb_security_groups = [module.securitygroups.aws_lb_security_group_id]
  lb_subnets         = [module.networking.aws_subnet1_id, module.networking.aws_subnet2_id]
  certificate_arn    = var.certificate_arn
  aws_vpc_id         = module.networking.vpc_id
  aws_instance_id1   = module.compute.webservers_instance1_id
  aws_instance_id2   = module.compute.webservers_instance2_id
}

module "efs" {
  source           = "./modules/efs"
  performance_mode = var.performance_mode
  throughput_mode  = var.throughput_mode
  efs_subnet_id1   = module.networking.aws_subnet3_id
  efs_subnet_id2   = module.networking.aws_subnet4_id
}


/*
terraform {
  backend "s3" {
    bucket = "magento-architecture-state"
    key    = "terraform/terraform.tfstate"
    region = "us-east-1"
  }
}
*/
