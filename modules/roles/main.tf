resource "aws_iam_role" "webserversrole" {
  name = "webserversrole"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
          "Service": [
            "ec2.amazonaws.com",
            "s3.amazonaws.com",
            "codedeploy.amazonaws.com",
            "cloudwatch.amazonaws.com"
          ]
        },
        "Effect": "Allow",
        "Sid": ""
      }
    ]
  }
  EOF

  tags = {
    Name = "webserversrole"
  }
}

