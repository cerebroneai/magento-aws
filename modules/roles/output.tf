output "webservers_role_id" {
  value = aws_iam_role.webserversrole.id
}

output "webserver_role_name" {
  value = aws_iam_role.webserversrole.name
}
