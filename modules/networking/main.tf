resource "aws_vpc" "main" {
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"
  tags = {
    Name = "main"
  }
}


#Public Subnet 1 in AZ1
resource "aws_subnet" "main-public-nat-2c" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.subnet1_cidr
  map_public_ip_on_launch = "true"
  availability_zone       = var.subnet1_az
  tags = {
    Name = "public-subnet-1"
  }
}

#Public Subnet 2 in AZ2
resource "aws_subnet" "main-public-nat-2a" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.subnet2_cidr
  map_public_ip_on_launch = "true"
  availability_zone       = var.subnet2_az
  tags = {
    Name = "public-subnet-2"
  }
}

#Private Subnet 1 in AZ1
resource "aws_subnet" "main-private-webserver-2c" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.subnet3_cidr
  map_public_ip_on_launch = "false"
  availability_zone       = var.subnet3_az
  tags = {
    Name = "private-subnet-1"
  }
}

#Private Subnet 2 in AZ2
resource "aws_subnet" "main-private-webserver-2a" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.subnet4_cidr
  map_public_ip_on_launch = "false"
  availability_zone       = var.subnet4_az
  tags = {
    Name = "private-subnet-2"
  }
}

#Private Subnet 3 in AZ2
resource "aws_subnet" "main-private-rds-2a" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.subnet5_cidr
  map_public_ip_on_launch = "false"
  availability_zone       = var.subnet5_az
  tags = {
    Name = "private-subnet-3"
  }
}

#Private Subnet 4 in AZ1
resource "aws_subnet" "main-private-rds-2c" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.subnet6_cidr
  map_public_ip_on_launch = "false"
  availability_zone       = var.subnet6_az
  tags = {
    Name = "private-subnet-4"
  }
}


#Internet-Gateway
resource "aws_internet_gateway" "main-gw" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "main-gw"
  }
}

#Route-Tables
resource "aws_route_table" "route-public" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main-gw.id
  }
  tags = {
    Name = "route-public"
  }
}

#Route-Table-Association with 2 Public Subnets In Two AZs
resource "aws_route_table_association" "main-public-2c-nat" {
  subnet_id      = aws_subnet.main-public-nat-2c.id
  route_table_id = aws_route_table.route-public.id
}

resource "aws_route_table_association" "main-public-2a-nat" {
  subnet_id      = aws_subnet.main-public-nat-2a.id
  route_table_id = aws_route_table.route-public.id
}

#EIP For NAT Instance
resource "aws_eip" "nat" {
  vpc      = true
  instance = var.nat_instance_id //aws_instance.NATInstance.id
  tags = {
    Name = "NatEIP"
  }
}

#Private Route Table For WebServers Instance1 in AZ1
resource "aws_route_table" "main-private-route1" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block  = "0.0.0.0/0"
    instance_id = var.nat_instance_id //aws_instance.NATInstance.id
  }
  tags = {
    Name = "main-private-route1"
  }
}

#Route Association Private Subnet1 AZ1 
resource "aws_route_table_association" "main-private-2c-webserver" {
  subnet_id      = aws_subnet.main-private-webserver-2c.id
  route_table_id = aws_route_table.main-private-route1.id
}

#Route Association Private Subnet2 AZ2
resource "aws_route_table_association" "main-private-2a-webserver" {
  subnet_id      = aws_subnet.main-private-webserver-2a.id
  route_table_id = aws_route_table.main-private-route1.id
}

#Route Association Private Subnet3 AZ2
resource "aws_route_table_association" "main-private-2a-rds" {
  subnet_id      = aws_subnet.main-private-rds-2a.id
  route_table_id = aws_route_table.main-private-route1.id
}

#Route Association Private Subnet4 AZ1
resource "aws_route_table_association" "main-private-2c-rds" {
  subnet_id      = aws_subnet.main-private-rds-2c.id
  route_table_id = aws_route_table.main-private-route1.id
}

/*
#VPC DHCP Options
resource "aws_vpc_dhcp_options" "dhcp" {
  domain_name          = var.dhcp_domain_name
  domain_name_servers  = var.dhcp_name_servers
  ntp_servers          = var.dhcp_ntp_servers
  netbios_name_servers = var.dhcp_netbios_name_servers
  netbios_node_type    = var.dhcp_netbios_node_type

  tags = {
    Name = "dhcp"
  }
}
#DHCP Options Associations
resource "aws_vpc_dhcp_options_association" "dns_resolver" {
  vpc_id          = "${aws_vpc.main.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.dhcp.id}"
}

*/
