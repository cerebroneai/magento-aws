
variable "vpc_cidr" {
  type        = string
  description = "CIDR for the VPC"

}

variable "subnet1_az" {
  type        = string
  description = "AZ for the subnet1"
}
variable "subnet1_cidr" {
  type        = string
  description = "CIDR for the subnet1"
}

variable "subnet2_az" {
  type        = string
  description = "AZ for the subnet2"
}
variable "subnet2_cidr" {
  type        = string
  description = "CIDR for the subnet2"
}

variable "subnet3_az" {
  type        = string
  description = "AZ for the subnet3"
}
variable "subnet3_cidr" {
  type        = string
  description = "CIDR for the subnet3"
}

variable "subnet4_az" {
  type        = string
  description = "AZ for the subnet4"
}
variable "subnet4_cidr" {
  type        = string
  description = "CIDR for the subnet4"
}

variable "subnet5_az" {
  type        = string
  description = "AZ for subnet5"
}
variable "subnet5_cidr" {
  type        = string
  description = "CIDR for subnet5"
}

variable "subnet6_az" {
  type        = string
  description = "AZ for subnet6"
}
variable "subnet6_cidr" {
  type        = string
  description = "CIDR for subnet6"
}

variable "nat_instance_id" {}

/*
variable "env" {}

variable "aws_region" {
  type        = string
  description = "Region for the VPC"
}

variable "vpc_name" {
  type        = string
  description = "NAME for the VPC"
}
#Public Subnet 1 AZ1 NAT
variable "subnet1_name" {
  type        = string
  description = "NAME for the subnet1"
}


#Public Subnet 2 AZ2 NAT
variable "subnet2_name" {
  type        = string
  description = "NAME for the subnet2"
}


#Private Subnet 3 AZ1 WebServer (Subnet1 PVT)
variable "subnet3_name" {
  type        = string
  description = "NAME for the subnet3"
}


#Private Subnet 4 AZ2 Webserver (Subnet2 PVT)
variable "subnet4_name" {
  type        = string
  description = "NAME for the subnet4"
}


#Private Subnet 5 AZ1 (Subnet3 PVT)
variable "subnet5_name" {
  type        = string
  description = "Name for the subnet5"
}
variable "subnet5_az" {
  type        = string
  description = "AZ for subnet5"
}
variable "subnet5_cidr" {
  type        = string
  description = "CIDR for subnet5"
}

#Private Subnet 6 AZ2 (Subnet4 PVT)
variable "subnet6_name" {
  type        = string
  description = "Name for the subnet6"
}
variable "subnet6_az" {
  type        = string
  description = "AZ for subnet6"
}
variable "subnet6_cidr" {
  type        = string
  description = "CIDR for subnet6"
}

#DHCP Variables
variable "dhcp_domain_name" {}
variable "dhcp_name_servers" {}
variable "dhcp_ntp_servers" {}
variable "dhcp_netbios_name_servers" {}
variable "dhcp_netbios_node_type" {}
*/
