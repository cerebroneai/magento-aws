output "vpc_id" {
  value = aws_vpc.main.id
}

output "aws_subnet1_id" {
  value = aws_subnet.main-public-nat-2c.id
}

output "aws_subnet3_id" {
  value = aws_subnet.main-private-webserver-2c.id
}

output "aws_subnet2_id" {
  value = aws_subnet.main-public-nat-2a.id
}

output "aws_subnet4_id" {
  value = aws_subnet.main-private-webserver-2a.id
}

output "aws_subnet5_id" {
  value = aws_subnet.main-private-rds-2a.id
}
output "aws_subnet6_id" {
  value = aws_subnet.main-private-rds-2c.id
}

output "default_security_group_id" {
  description = "The ID of the security group created by default on VPC creation"
  value       = concat(aws_vpc.main.*.default_security_group_id, [""])[0]
}
