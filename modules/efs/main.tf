resource "aws_efs_file_system" "EFSFileSystem" {
  performance_mode = var.performance_mode
  encrypted        = false
  throughput_mode  = var.throughput_mode
  tags = {
    Name = "walmart-dev"
  }
}

resource "aws_efs_mount_target" "target1" {
  file_system_id = aws_efs_file_system.EFSFileSystem.id
  subnet_id      = var.efs_subnet_id1
}

resource "aws_efs_mount_target" "target2" {
  file_system_id = aws_efs_file_system.EFSFileSystem.id
  subnet_id      = var.efs_subnet_id2
}
