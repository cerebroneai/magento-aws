
# Define the security group for public subnet
resource "aws_security_group" "securityloadbalancer" {
  name        = "loadbalancer"
  description = "Allow incoming HTTPS connections & SSH access for application"

  ingress {
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 80
    protocol         = "tcp"
    to_port          = 80
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 443
    protocol         = "tcp"
    to_port          = 443
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = var.vpc_id

  tags = {
    Name = "securityloadbalancer"
  }

}

//for ssh into nat instance 
resource "aws_security_group" "natssh" {
  name        = "webservers"
  description = "Security Groups For  SSH"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.ingress_cidr_blocks
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.ingress_cidr_blocks
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = var.ingress_cidr_blocks
  }
  ingress {
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = var.ingress_cidr_blocks
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  vpc_id = var.vpc_id
  tags = {
    Name = "natssh"
  }
}

# Define the security group for RDS in Private Subnet
resource "aws_security_group" "sgprivatedb" {
  name        = "database"
  description = "Allow traffic from private subnet for db access"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = var.cidr_blocks
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.cidr_blocks
  }

  egress {
    protocol    = "tcp"
    from_port   = 443
    cidr_blocks = ["0.0.0.0/0"]
    to_port     = 443
  }
  vpc_id = var.vpc_id

  tags = {
    Name = "sgprivatedb"
  }
}

#Define the Security Group For NAT Instance In the Public Subnet
resource "aws_security_group" "nat" {
  vpc_id      = var.vpc_id
  name        = "nat"
  description = "security groups for allowing ssh and traffic"
  ingress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = var.cidr_blocks
  }
  ingress {
    protocol        = -1
    from_port       = 0
    to_port         = 0
    security_groups = [aws_security_group.natssh.id]
  }
  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "nat"
  }
}


