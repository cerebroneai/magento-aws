
variable "cidr_blocks" {
  type        = list(string)
  description = "CIDR for the Security Groups"
  default     = []

}

variable "vpc_id" {}
variable "vpc_cidr" {
  type = string
}

variable "ingress_cidr_blocks" {
  type        = list(string)
  description = "CIDR for Security Groups"
}
