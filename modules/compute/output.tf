output "ec2_tag_value" {
  value = aws_instance.EC2Webserver1.tags.Name
}

output "nat_instance_id" {
  value = aws_instance.NATInstance.id
}

output "webservers_instance1_id" {
  value = aws_instance.EC2Webserver1.id
}

output "webservers_instance2_id" {
  value = aws_instance.EC2Webserver2.id
}
