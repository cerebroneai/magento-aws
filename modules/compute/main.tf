// AWS INSTANCE WITHOUT AUTOSCALING
resource "aws_instance" "EC2Webserver1" {
  ami                    = var.ec2_ami
  instance_type          = var.ec2_instance_type
  vpc_security_group_ids = var.vpc_security_group_ids
  subnet_id              = var.subnet_id1 //us-east-1c
  key_name               = var.key_name
  availability_zone      = var.availability_zone1 //us-east-1c
  ebs_optimized          = var.ebs_optimised      //boolean variable (default value as false)
  tenancy                = "default"

  ebs_block_device {
    device_name           = var.ebs_block_name
    volume_type           = var.ec2_volume_type
    encrypted             = true
    volume_size           = var.ec2_volume_size
    delete_on_termination = true
    snapshot_id           = aws_ebs_snapshot.volume_snapshot1.id
  }

  root_block_device {
    volume_size           = var.root_volume_size
    volume_type           = var.root_volume_type
    delete_on_termination = true

  }

  iam_instance_profile = aws_iam_instance_profile.iamwebservers.name
  monitoring           = true

  tags = {
    Name = "EC2Webserver1"
  }
}

// AWS INSTANCE2 WITHOUT AUTOSCALING
resource "aws_instance" "EC2Webserver2" {
  ami                    = var.ec2_ami
  instance_type          = var.ec2_instance_type
  vpc_security_group_ids = var.vpc_security_group_ids
  subnet_id              = var.subnet_id2 //us-east-1a
  key_name               = var.key_name
  availability_zone      = var.availability_zone2 //us-east-1a
  ebs_optimized          = false
  tenancy                = "default"

  ebs_block_device {
    device_name           = var.ebs_block_name
    encrypted             = true
    volume_type           = var.ec2_volume_type
    volume_size           = var.ec2_volume_size
    delete_on_termination = true
    snapshot_id           = aws_ebs_snapshot.volume_snapshot2.id
  }

  root_block_device {
    volume_size           = var.root_volume_size
    volume_type           = var.root_volume_type
    delete_on_termination = true

  }
  iam_instance_profile = aws_iam_instance_profile.iamwebservers.name
  monitoring           = true
  tags = {
    Name = "EC2Webserver2"
  }
}


#IAM Instance Profile
resource "aws_iam_instance_profile" "iamwebservers" {
  name = "iamwebservers"
  role = var.role
}

#EBS BLOCK1
resource "aws_ebs_volume" "volwebservers1" {
  availability_zone = "us-east-1c"
  size              = 40

  tags = {
    Name = "volwebservers1"
  }
}

resource "aws_ebs_snapshot" "volume_snapshot1" {
  volume_id = aws_ebs_volume.volwebservers1.id

  tags = {
    Name = "webservers_snap1"
  }
}

#EBS BLOCK2
resource "aws_ebs_volume" "volwebservers2" {
  availability_zone = "us-east-1a"
  size              = 40

  tags = {
    Name = "volwebservers2"
  }
}

resource "aws_ebs_snapshot" "volume_snapshot2" {
  volume_id = aws_ebs_volume.volwebservers2.id

  tags = {
    Name = "webservers_snap2"
  }
}

#NAT Instance in 1st Public Subnet For Route Association With Private Subnets
resource "aws_instance" "NATInstance" {
  ami                    = var.nat_ami
  instance_type          = var.nat_instance_type
  key_name               = var.key_name
  subnet_id              = var.nat_subnet_id //aws_subnet.main-public-nat-2c.id
  vpc_security_group_ids = var.nat_vpc_security_group_ids
  source_dest_check      = false
  ebs_optimized          = false

  root_block_device {
    volume_size           = var.nat_volume_size
    volume_type           = var.nat_volume_type
    delete_on_termination = true
  }
  tags = {
    Name = "NATInstance"
  }
}
