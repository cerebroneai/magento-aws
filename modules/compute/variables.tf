variable "vpc_security_group_ids" {}
variable "ec2_volume_type" {}
variable "ec2_volume_size" {}
variable "ec2_instance_type" {}
variable "subnet_id1" {}
variable "subnet_id2" {}
variable "key_name" {}
variable "ec2_ami" {}
variable "availability_zone1" {}
variable "availability_zone2" {}
variable "root_volume_size" {}
variable "root_volume_type" {}
variable "role" {}
variable "ebs_block_name" {}
#set to true for sppecified instance
variable "ebs_optimised" {}


variable "nat_ami" {}
variable "nat_instance_type" {}
variable "nat_volume_size" {}
variable "nat_volume_type" {}
variable "nat_vpc_security_group_ids" {}
variable "nat_subnet_id" {}
