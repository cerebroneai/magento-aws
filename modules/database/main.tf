#RDS KMS KEY
resource "aws_kms_key" "RDSKMS" {
  description             = "KMS key"
  deletion_window_in_days = var.deletion_window_in_days
  tags = {
    Name = "RDSKMS"
  }
}

#RDS Private Subnet Group
resource "aws_db_subnet_group" "dbsubnet" {
  name       = "rds_db subnet group"
  subnet_ids = var.subnet_ids
  tags = {
    Name = "dbsubnet"
  }
}

#RDS IN 2 Private Subnets
resource "aws_db_instance" "RDS1" {
  allocated_storage                   = var.allocated_storage
  identifier                          = var.db_identifier
  storage_type                        = var.storage_type
  engine                              = var.database_engine
  engine_version                      = var.database_engine_version
  instance_class                      = var.instance_class
  multi_az                            = true //check this
  name                                = var.db_name
  username                            = var.db_username
  password                            = var.db_password
  db_subnet_group_name                = aws_db_subnet_group.dbsubnet.name
  publicly_accessible                 = false
  vpc_security_group_ids              = [var.db_security_group]
  storage_encrypted                   = true
  iam_database_authentication_enabled = false
  deletion_protection                 = false
  auto_minor_version_upgrade          = true
  monitoring_interval                 = var.monitoring_interval
  license_model                       = var.license_model
  maintenance_window                  = var.maintenance_window
  backup_window                       = var.backup_window
  backup_retention_period             = var.backup_retention_period
  kms_key_id                          = aws_kms_key.RDSKMS.arn
  skip_final_snapshot                 = "true"
  copy_tags_to_snapshot               = false
  tags = {
    Name = "RDS1"
  }
}

