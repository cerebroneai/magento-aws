variable "database_engine_version" {}
variable "db_identifier" {}
variable "allocated_storage" {}
variable "storage_type" {}
variable "database_engine" {}
variable "instance_class" {}
variable "db_name" {}
variable "db_username" {}
variable "db_password" {}
variable "deletion_window_in_days" {}
variable "db_security_group" {}
variable "license_model" {}
variable "maintenance_window" {}
variable "monitoring_interval" {}
variable "backup_window" {}
variable "backup_retention_period" {}
variable "subnet_ids" {}

