resource "aws_lb" "loadbalancer" {
  name               = "loadbalancer"
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.lb_security_groups
  subnets            = var.lb_subnets
  ip_address_type    = "ipv4"
  access_logs {
    enabled = "false"
    bucket  = ""
    prefix  = ""
  }
  idle_timeout               = "60"
  enable_deletion_protection = "false"
  enable_http2               = "true"

  tags = {
    Name = "LoadBalancer"
  }
}

resource "aws_lb_target_group" "lb_target_group" {
  name     = "ec2-target-group"
  port     = 443
  protocol = "HTTPS"
  vpc_id   = var.aws_vpc_id

}

resource "aws_lb_target_group_attachment" "lb-attachment1" {
  target_group_arn = aws_lb_target_group.lb_target_group.arn
  target_id        = var.aws_instance_id1 //aws_instance.WebServer.id
  port             = 443
}

resource "aws_lb_target_group_attachment" "lb-attachment2" {
  target_group_arn = aws_lb_target_group.lb_target_group.arn
  target_id        = var.aws_instance_id2 //aws_instance.WebServer.id
  port             = 443
}

resource "aws_lb_listener" "lb_listner1" {
  load_balancer_arn = aws_lb.loadbalancer.arn
  port              = 443
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = var.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lb_target_group.arn
  }
}

resource "aws_lb_listener" "lb_listner2" {
  load_balancer_arn = aws_lb.loadbalancer.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lb_target_group.arn
  }
}

