aws_region = "us-east-1"


#Networking
vpc_cidr = "10.0.0.0/16"
cidr_blocks = ["10.0.0.0/16"]
ingress_cidr_blocks = ["10.0.0.0/16","10.0.0.0/16","10.0.0.0/16"]

subnet1_az = "us-east-1c"
subnet2_az = "us-east-1a"
subnet3_az = "us-east-1c"
subnet4_az = "us-east-1a"
subnet5_az = "us-east-1c"
subnet6_az = "us-east-1a"

subnet1_cidr = "10.0.14.0/24"
subnet2_cidr = "10.0.13.0/24"
subnet3_cidr = "10.0.3.0/24"
subnet4_cidr = "10.0.15.0/24"
subnet5_cidr = "10.0.5.0/24"
subnet6_cidr = "10.0.4.0/24"


#Database
database_engine_version = "5.6.34"
db_identifier = "dev-walmartb2b"
allocated_storage = 20
storage_type = "gp2"
database_engine = "mysql"
instance_class = "db.t3.large"
db_name = "Walmart"
db_username = "uat_dbo"
db_password = "REPLACEME"
license_model = "general-public-license"
deletion_window_in_days = 30
maintenance_window = "sun:07:00-sun:08:00"
monitoring_interval = 0
backup_window = "05:00-06:00"
backup_retention_period = 7



#Compute
nat_ami = "ami-00a9d4a05375b2763"
nat_instance_type = "t2.nano" 
nat_volume_size = 8
nat_volume_type = "standard"
key_name = "mykey"
public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCl4b2q2aiwl4VEkxY1/BuhlJ/L61v/G4Z5S3HgqxuUAGsbvLJQcQ/2+Ge5trEVNUrP/nPVCADxQg4L7pn49Jak5P8UKUFZOGkhChXZ50KpWgU49133MAIYVf6tM+isNjZg78G+quVXtlfHWRQOi3iAyTwAijNwR+9jQlAUgOV8eF9qjwL3a7H9Pb+kA+oirBGC/+s5aqRqmEVevkJfcTag1Shgg7smkKTbywxKDpcvcsJngEovEV8bWuelBETW3S+uz6WDJZhNbps9G3kkTYlhgJAWF1fuK3Jy+Fpf4Cw42eAma/Ki7oIoWDle4HjFOcEIZWEEdi1CrX8KrJOxz94NcnSor2UYpLIgajmAdE2ZuRzneAGQEMso0+mCbdqwoE3JSOrAOpdXGahzSaH3eg8N8L3moP+mKhZ6lcP9GbDEl8t3BAhD0DnK6gUjAk7vOYVwotuMy3Yv+imHHygrmZdL1MbIV0RPHHru+8ylRiv1Rv6Y2x5wvVFmBZHm1DRZRAM= abhishek@abhishek-HP-Notebook"
ec2_volume_type = "gp2"
ec2_volume_size = 200
ec2_instance_type = "t2.micro"
#Replace the AMI ID With "ami-0c5b83c32ccd8343e"
ec2_ami = "ami-0947d2ba12ee1ff75"
availability_zone1 = "us-east-1c"
availability_zone2 = "us-east-1a"
root_volume_size = 200
root_volume_type = "gp2"
ebs_block_name   = "/dev/sdb"
#set to true for sppecified instance
ebs_optimised = false



#Cache
node_type            = "cache.t2.small"
engine               = "memcached"
engine_version       = "1.4.5"
cluster_id           = "walmart-dev"
num_cache_nodes      = 1
cluster_availability_zone    = "us-east-1a"
cluster_maintenance_window   = "tue:05:00-tue:06:00"
parameter_group_name = "default.memcached1.4"
 

#LoadBalancer
#Need to attach a private certificate
certificate_arn = "arn:aws:acm:us-east-1:680763698946:certificate/cc0c6f97-7e13-414b-a526-f6571beb7884"


#EFS
performance_mode = "generalPurpose"
throughput_mode =   "bursting"