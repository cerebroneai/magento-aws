variable "aws_region" {}


#Networking
variable "vpc_cidr" {
  type        = string
  description = "CIDR for the VPC"

}

variable "cidr_blocks" {
  type        = list(string)
  description = "CIDR for Security Groups"
}

variable "ingress_cidr_blocks" {
  type        = list(string)
  description = "CIDR for Security Groups"
}

variable "subnet1_az" {}
variable "subnet1_cidr" {}

variable "subnet2_az" {}
variable "subnet2_cidr" {}

variable "subnet3_az" {}
variable "subnet3_cidr" {}

variable "subnet4_az" {}
variable "subnet4_cidr" {}

variable "subnet5_az" {}
variable "subnet5_cidr" {}

variable "subnet6_az" {}
variable "subnet6_cidr" {}


#Database
variable "database_engine_version" {}
variable "db_identifier" {}
variable "allocated_storage" {}
variable "storage_type" {}
variable "database_engine" {}
variable "instance_class" {}
variable "db_name" {}
variable "db_username" {}
variable "db_password" {}
variable "deletion_window_in_days" {}
variable "license_model" {}
variable "maintenance_window" {}
variable "monitoring_interval" {}
variable "backup_window" {}
variable "backup_retention_period" {}


#Compute
variable "nat_ami" {}
variable "nat_instance_type" {}
variable "key_name" {}
variable "nat_volume_size" {}
variable "nat_volume_type" {}
variable "public_key" {}
variable "ec2_volume_type" {}
variable "ec2_volume_size" {}
variable "ec2_instance_type" {}
variable "ec2_ami" {}
variable "availability_zone1" {}
variable "availability_zone2" {}
variable "root_volume_size" {}
variable "root_volume_type" {}
variable "ebs_block_name" {}
#set to true for sppecified instance
variable "ebs_optimised" {}


#Cache
variable "engine" {}
variable "node_type" {}
variable "engine_version" {}
variable "cluster_maintenance_window" {}
variable "num_cache_nodes" {}
variable "parameter_group_name" {}
variable "cluster_availability_zone" {}
variable "cluster_id" {}

#LoadBalancer
variable "certificate_arn" {}

#EFS
variable "performance_mode" {}
variable "throughput_mode" {}
